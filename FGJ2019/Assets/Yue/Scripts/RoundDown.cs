﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundDown : MonoBehaviour
{
    public static RoundDown instance;
    public RoundDowns m_RoundDowns = RoundDowns.Start;
    public bool isPlayerCanMove;

    private void Awake()
    {
        if(instance == null)
        instance = this;
    }

    private void Start()
    {
        isPlayerCanMove = true;
        OnGO();
    }


    public enum RoundDowns
    {
        Start = 0,
        GoToTV = 10,
        ShowParent = 20,
        ClickParent = 25,
        LaughParent = 27,
        LaughParent2 = 28,
        EndShowParent = 30,
        FindTeacher = 35,
        TouchTeacher = 40,
        EndTouchTeacher = 50,
        TouchTeacherMirror = 60,
        TouchSinger = 70,


    }

    public void OnGO()
    {
        if(m_RoundDowns == RoundDowns.Start)
        {
            isPlayerCanMove = true;


        }
        else if (m_RoundDowns == RoundDowns.GoToTV)
        {

        }
        else if(m_RoundDowns == RoundDowns.ShowParent)
        {
            UIManager.instance.OnShowNarration(3, "爸爸爸媽媽媽爸爸爸媽媽媽");
        }

        else if (m_RoundDowns == RoundDowns.ClickParent)
        {
            UIManager.instance.OnShowNarration(2, "我 :我今天考100分");
            m_RoundDowns = RoundDowns.LaughParent;
            OnGO();
        }
        else if (m_RoundDowns == RoundDowns.LaughParent)
        {
            Debug.Log("撥音效 爸爸嬤嬤笑");
            m_RoundDowns = RoundDowns.FindTeacher;
            OnGO();
        }
        else if(m_RoundDowns == RoundDowns.FindTeacher)
        {
            ObjectManager.instance.afterTVBlock.SetActive(false);
            //玩家要自己去碰老師
        }

        else if (m_RoundDowns == RoundDowns.TouchTeacher)
        {
            //碰到老師
            ObjectManager.instance.afterTVBlock.SetActive(true);
            ObjectManager.instance.startTV.SetActive(false);
            UIManager.instance.OnShowNarration(3, "旁白 :成為老師好榜樣");
            Debug.Log("環境音 學校同學窸窸窣窣");
            StartCoroutine(WaitingTimer(3));
            m_RoundDowns = RoundDowns.EndTouchTeacher;
        }

        else if (m_RoundDowns == RoundDowns.EndTouchTeacher)
        {
            //跳出穿堂竟
            UIManager.instance.OnShowNarration(3, "旁白 :跟上流行的話題，裝扮成受歡迎的模樣");
            ObjectManager.instance.mirrorAfterTeacher.SetActive(true);
            //打開牆壁
            ObjectManager.instance.AfterTeacherBlock.SetActive(false);
        }

        else if (m_RoundDowns == RoundDowns.TouchTeacherMirror)
        {
            //點擊到鏡子
            UIManager.instance.OnShowNarration(3, "旁白 :跟上流行的話題，裝扮成受歡迎的模樣");
            //關後面牆壁
            ObjectManager.instance.AfterTeacherBlock.SetActive(true);

            //打開下一個牆壁
            ObjectManager.instance.AfterTouchTMirrorBlock.SetActive(false);

        }


    }

    IEnumerator WaitingTimer(float _timer)
    {
        yield return new WaitForSeconds(_timer);
        OnGO();

    }

    void Hint()
    {
        //變黑2秒
        //UIManager.instance.OnFadeInOutBG(2, true);
        //設定X秒後 顯示文字XXX X秒鐘後消失
        //UIManager.instance.SetStroyText(2, 3, "Pizza很好吃嗎?Pizza很好吃嗎?Pizza很好吃嗎?");
    }


}
