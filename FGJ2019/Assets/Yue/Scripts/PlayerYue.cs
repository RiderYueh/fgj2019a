﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerYue : MonoBehaviour
{
    public LayerMask clickLayer = 1;
    bool isCanClick;
    public Camera mainCam;

    void Start()
    {
        isCanClick = true;
    }

    void Update()
    {
        if (isCanClick) CheckClick();
    }

    void CheckClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, mainCam.nearClipPlane));
            if (Physics.Raycast(ray, out hit, 2 , clickLayer))
            {
                Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object

                if(hit.transform.name == "sofa")
                {
                    if(RoundDown.instance.m_RoundDowns == RoundDown.RoundDowns.ShowParent)
                    {
                        RoundDown.instance.m_RoundDowns = RoundDown.RoundDowns.ClickParent;
                        RoundDown.instance.OnGO();
                    }
                    
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ShowText")
        {
            other.GetComponent<ShowText>().OnPlayerTouch();
        }

        else if(other.tag == "OpenSofa")
        {
            if(RoundDown.instance.m_RoundDowns == RoundDown.RoundDowns.Start)
            {
                ObjectManager.instance.sofa.SetActive(true);
                RoundDown.instance.m_RoundDowns = RoundDown.RoundDowns.ShowParent;
                RoundDown.instance.OnGO();
                Debug.Log("趴 出現爸媽");
            }
        }

        else if (other.tag == "TouchTeacher")
        {
            if (RoundDown.instance.m_RoundDowns == RoundDown.RoundDowns.FindTeacher)
            {
                Debug.Log("趴 碰到老師");
                RoundDown.instance.m_RoundDowns = RoundDown.RoundDowns.TouchTeacher;
                RoundDown.instance.OnGO();
                
            }
        }
        else if (other.tag == "MirrorFin")
        {
            if (RoundDown.instance.m_RoundDowns == RoundDown.RoundDowns.EndTouchTeacher)
            {
                Debug.Log("碰到鏡子");
                RoundDown.instance.m_RoundDowns = RoundDown.RoundDowns.TouchTeacherMirror;
                RoundDown.instance.OnGO();

            }
        }
        else if (other.tag == "TouchSinger")
        {
            if (RoundDown.instance.m_RoundDowns == RoundDown.RoundDowns.TouchTeacherMirror)
            {
                Debug.Log("碰到點唱機");
                RoundDown.instance.m_RoundDowns = RoundDown.RoundDowns.TouchSinger;
                RoundDown.instance.OnGO();

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ShowText")
        {
            other.GetComponent<ShowText>().OnPlayerExit();
        }
    }
}
