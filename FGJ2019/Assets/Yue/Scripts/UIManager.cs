﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static public UIManager instance;
    public Image BGImage;
    public Text StoryText;

    public GameObject Narration;
    public Text NarrationText;

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }


    private void Start()
    {
        
    }

    public void OnShowNarration(float _timer , string _text)
    {
        StartCoroutine(ShowNarration(_timer, _text));
    }


    public void OnFadeInOutBG(float timer, bool isFadeIn)
    {
        BGImage.enabled = true;
        StartCoroutine(FadeInOutBG(timer , isFadeIn));
    }

    //等待幾秒後開始顯示文字  顯示幾秒  文字內容
    public void SetStroyText(float waitToShowTimer, float stayTimer , string strings)
    {
        StoryText.text = "";
        StoryText.enabled = true;
        StartCoroutine(PlayTextAndSound(waitToShowTimer, stayTimer, strings));
    }


    //把背景與文字清除
    public void OnFadeOutAndClearText()
    {
        OnFadeInOutBG(1, false);
        StoryText.text = "";
        StoryText.enabled = false;
    }
    
    IEnumerator PlayTextAndSound(float waitToShowTimer, float stayTimer, string strings)
    {
        for (float i = 0; i < waitToShowTimer; i += Time.deltaTime)
        {
            yield return 0;
        }

        print("撥語音效");

        StoryText.text = strings;

        yield return new WaitForSeconds(stayTimer);

        OnFadeOutAndClearText();
    }

    IEnumerator FadeInOutBG(float timer , bool isFadeIn)
    {
        if(isFadeIn)
        {
            BGImage.color = new Color(BGImage.color.r, BGImage.color.g, BGImage.color.b, 0);
            for (float i = 0; i < timer; i += Time.deltaTime)
            {
                BGImage.color = new Color(BGImage.color.r, BGImage.color.g, BGImage.color.b, i / timer);
                yield return 0;
            }
        }
        else
        {
            BGImage.color = new Color(BGImage.color.r, BGImage.color.g, BGImage.color.b, 1);
            for (float i = 0; i < timer; i += Time.deltaTime)
            {
                BGImage.color = new Color(BGImage.color.r, BGImage.color.g, BGImage.color.b ,1 - (i / timer));
                yield return 0;
            }
        }
        
    }

    //旁白
    IEnumerator ShowNarration(float _timer, string strings)
    {
        print("撥語旁白");
        Narration.SetActive(true);
        NarrationText.text = strings;

        for (float i = 0; i < _timer; i += Time.deltaTime)
        {
            yield return 0;
        }
        //結束旁白視窗
        OnNarrationEnd();
    }

    void OnNarrationEnd()
    {
        Narration.SetActive(false);
        NarrationText.text = "";
    }
}
