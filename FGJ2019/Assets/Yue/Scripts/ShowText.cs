﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowText : MonoBehaviour
{
    public TextMesh m_TextMesh;
    public string text1 = "";
    public string text2 = "";
    public string text3 = "";

    private void Start()
    {
    }

    public void OnPlayerTouch()
    {
		m_TextMesh.GetComponent<TextMesh>().text = text1;
    }

    public void OnPlayerExit()
    {
        m_TextMesh.text = text2;
        Debug.Log("播音笑");
        //MainManager.instance.CreatParticle(transform.position , "tauntlaugh");
    }
}
