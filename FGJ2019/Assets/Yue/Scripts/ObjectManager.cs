﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    static public ObjectManager instance;

    public GameObject startTV;
    public GameObject sofa;
    public GameObject afterTVBlock;
    public GameObject teacher;
    public GameObject mirrorAfterTeacher;
    public GameObject mirrorReflectionAfterTeacher;
    public GameObject AfterTeacherBlock;
    public GameObject AfterTouchTMirrorBlock;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
}
