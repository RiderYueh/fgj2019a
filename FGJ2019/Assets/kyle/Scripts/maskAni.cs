﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maskAni : MonoBehaviour
{
	public Animation anim;
	public string animationName;

    // Start is called before the first frame update
    void Start()
    {
		if(anim)
		anim.enabled = false;
    }

	void StartAni()
	{
		//if(Input.GetKeyDown(KeyCode.Space)){
		//	Debug.Log("play animation : " + animationName);
			anim.enabled = true;
			anim.Play(animationName);
		//}
	}

    // Update is called once per frame
    void Update()
    {
		//StartAni();
    }
}
