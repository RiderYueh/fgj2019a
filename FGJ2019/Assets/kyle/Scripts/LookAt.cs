﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
	
	private Transform target;
	public GameObject Neck;
	public Animator ani;

	void Start()
	{
		print( LookAtTarget.instance.target);
		target = LookAtTarget.instance.target.transform;
	}

	void Update()
	{
		if(target){
			print(ani);

			//transform.LookAt(target);
			Neck.transform.Rotate(0,10,0);
		}
		else{
			print("no target");
		}
	}


}
