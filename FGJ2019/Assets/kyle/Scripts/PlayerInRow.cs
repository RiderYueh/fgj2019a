﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInRow : MonoBehaviour
{

	public GameObject TriggerTarget;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == TriggerTarget.tag)
		{
			Debug.Log("在隊伍裡了");
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == TriggerTarget.tag)
		{
			Debug.Log("你脫隊了");
		}
	}

}
