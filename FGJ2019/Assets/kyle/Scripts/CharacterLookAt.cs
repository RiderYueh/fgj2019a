﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLookAt : MonoBehaviour
{
	private Transform target;
	public GameObject Neck;

	Quaternion startRotation, endRotation;
	float period, time;
	Vector3 point;

	void Start()
	{
		print( LookAtTarget.instance.target);
		target = LookAtTarget.instance.target.transform;
	}

	void LookAt(Vector3 point){
		print("LookAt");
		startRotation = transform.rotation;
		endRotation = Quaternion.LookRotation(transform.forward, point - transform.position);

		float time = 0f;
		float period = 3f;

		this.point = point;
		enabled = true;
	}

	void LateUpdate()
	{
		//point = Neck.transform.position;
		//cusLookAt(point);

		time += Time.deltaTime;
		if(time >= period){
			print("time >= period");
			Neck.transform.LookAt(point);
			enabled = false;
		}
		else{
			print("Slerp");
			transform.rotation = Quaternion.Slerp(startRotation, endRotation, time/period);
		}


	}
}
