﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowRun : MonoBehaviour
{
	public GameObject Row;
	public float rotateSpeed;
	public float forwardSpeed;
	public float forwardDistance;

	private float RightAngle = 0;
	private float goForward = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (goForward < forwardDistance) {
			float dis = forwardSpeed * Time.deltaTime;
			goForward += dis;
			Forward(dis);
		}
		else{
			float rotate = rotateSpeed * Time.deltaTime;
			RightAngle = RightAngle + rotate;
			if (RightAngle < 90){				
				TurnRight(rotate);
			}
			else{
				RightAngle = 0;
				goForward = 0;
				rotate = Row.transform.localRotation.y % 90;
				TurnRight(rotate);
			}
		}
    }

	void Forward(float dis){		
		Row.transform.Translate (0,0,dis);
	}

	void TurnRight(float rotate)
	{			
		Row.transform.Rotate(0, rotate ,0);
		Forward(rotate * 0.01f);
	}
}
