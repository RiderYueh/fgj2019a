﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallButton : MonoBehaviour
{
    public bool isShow;
    public GameObject wall;
    public float wallheight;
	public float wallHorizon;
    public float speed;
    // Start is called before the first frame update
    float h;
	float w;
    Vector3 target;
    void Start()
    {
        target = wall.transform.position;
        h = 0;
		w = 0;
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }
    void OnTriggerEnter(Collider col)
    {
        //Debug.Log("button trigger");
        //
        if (Vector3.Distance(wall.transform.position, target) > 0.1f) return;
        h = (isShow) ? wallheight : -wallheight;
		w = (isShow) ? wallHorizon : -wallHorizon;
        target = new Vector3(wall.transform.position.x + w, wall.transform.position.y + h, wall.transform.position.z);
        isShow = !isShow;
    }
    void move()
    {
        wall.transform.position = Vector3.MoveTowards(wall.transform.position, target, speed * Time.deltaTime);
    }

}
